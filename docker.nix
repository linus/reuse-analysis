let
  mach-nix = import (builtins.fetchGit {
    url = "https://github.com/DavHau/mach-nix";
    ref = "refs/tags/3.5.0";
  }) { };
in let
  image = mach-nix.mkDockerImage {
    requirements = ''
      pandas
      plotly
      streamlit
    '';
  };
in image.override (oldAttrs: {
  name = "linozen/reuse-analysis";
  tag = "0.1.0";
  config = {
    WorkingDir = "/data";
    Volumes = { "/data" = { }; };
    Cmd = [ "streamlit" "run" "/data/main.py" ];
  };
})
