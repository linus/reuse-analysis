{ pkgs ? import
  (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-22.05.tar.gz")
  { } }:

with pkgs;

mkShell {
  buildInputs = [
    csvs-to-sqlite
    python39Packages.black
    python39Packages.isort
    python39Packages.matplotlib
    python39Packages.pandas
    python39Packages.plotly
    python39Packages.requests
    python39Packages.tldextract
    sqlite-utils
    streamlit
  ];
}
