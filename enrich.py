#!/usr/bin/env python3

import os
import sqlite3
import regex as re
from urllib.parse import quote

import pandas as pd
import requests
import tldextract

# Set tokens for forge APIs
gh_username = os.getenv("GH_USERNAME")
gr_token = os.getenv("GH_TOKEN")

gl_username = os.getenv("GH_USERNAME")
gl_token = os.getenv("GH_TOKEN")


def get_slug(repo_url: str) -> str:
    slug = "/".join(repo_url.split("/")[1:])
    return str(slug)


def get_forge(repo_url: str) -> str:
    forge = ".".join([p for p in tldextract.extract(repo_url) if p != ""])
    return forge.lower()


def get_owner(repo_url: str) -> str:
    slug = get_slug(repo_url)
    forge = get_forge(repo_url)
    if forge in ["github.com", "gitlab.com", "git.sr.ht", "codeberg.org"]:
        owner = slug.split("/")[0]
    else:
        # This is a privately hosted git instance
        owner = forge

    # Some SAP-specific cleaning
    if owner.upper() in ["SAP", "SAP-SAMPLES", "SAP-DOCS"]:
        owner = "SAP"

    return owner


def get_stars_github(slug: str) -> int:
    res = requests.get(
        f"https://api.github.com/repos/{slug}", auth=(gh_username, gr_token)
    )
    try:
        stars = int(res.json()["stargazers_count"])
    except:
        stars = 0
    print(f"{slug} has {stars} on GitHub.")
    return stars


def get_stars_gitlab(slug: str) -> int:
    # GitLab needs encoded slugs
    slug_encoded = quote(slug, safe="")
    res = requests.get(
        f"https://gitlab.com/api/v4/projects/{slug_encoded}",
        auth=(gl_username, gl_token),
    )
    try:
        stars = int(res.json()["star_count"])
    except:
        stars = 0
    print(f"{slug} has {stars} on GitLab.")
    return stars


def get_stars(repo_url: str) -> int:
    forge = get_forge(repo_url).lower()
    slug = get_slug(repo_url)
    if forge == "github.com":
        stars = get_stars_github(slug)
        return stars
    elif forge == "gitlab.com":
        stars = get_stars_gitlab(slug)
        return stars
    else:
        return 0


def get_copyrighted_files(lint_output: str) -> int:
    found = re.findall("Files with copyright information: (.*) / .*\n", lint_output)
    try:
        files = int(found[0])
    except IndexError:
        files = 0
    return files


def get_licenses(lint_output: str):
    found = re.findall("Used licenses: (.*)\n", lint_output)
    try:
        licenses = [l.strip() for l in found[0].split(",")]
    except IndexError:
        licenses = None
    return licenses


def main():
    conn = sqlite3.connect("database.sqlite")
    df = pd.read_sql_query("SELECT * FROM repository", conn)
    # Enrich DataFrame
    df["copyrighted_files"] = df.apply(
        lambda row: get_copyrighted_files(row["lint_output"]), axis=1
    )
    df["used_licences"] = df.apply(lambda row: get_licenses(row["lint_output"]), axis=1)
    df["stars"] = df.apply(lambda row: get_stars(row["url"]), axis=1)
    df["forge"] = df.apply(lambda row: get_forge(row["url"]), axis=1)
    df["owner"] = df.apply(lambda row: get_owner(row["url"]), axis=1)
    df.to_csv("reuse.csv", index=False)
    return df


if __name__ == "__main__":
    main()
