#!/usr/bin/env python3

import ast
from math import floor, log

import pandas as pd
import plotly.express as px
import streamlit as st


# Utility functions to be cached
@st.cache()
def human_format(number):
    units = ["", "K", "M", "G", "T", "P"]
    k = 1000.0
    magnitude = int(floor(log(number, k)))
    return "%.2f%s" % (number / k ** magnitude, units[magnitude])


@st.cache()
def get_df() -> pd.DataFrame:
    df = pd.read_csv("reuse.csv")
    return df


def transform_to_list(some_input) -> list:
    try:
        return_val = ast.literal_eval(some_input)
    except:
        return_val = []
    return return_val


def make_one_dimensional(series):
    return_val = pd.Series([x for _list in series for x in _list])
    return return_val


# Title and page configuration
st.set_page_config(
    page_title="REUSE API Analysis",
    page_icon="✨",
    layout="wide",
)
st.title("Who is using the REUSE API?")

# Create DataFrame from SQLite DB
df = get_df()
df_unfiltered = df.query("status == 'compliant'")


# Sidebar
with st.sidebar:
    st.header("Filters")
    # --- REUSE compliance
    only_compliant = st.checkbox("Only show REUSE compliant projects", value=True)
    if only_compliant:
        df = df.query("status == 'compliant'")

    # --- Amount of Stars
    min_stars = st.slider(
        "Select minimum number of stars",
        0,
        1000,
        100,
    )
    df = df.query(f"stars >= {min_stars}")

    # --- Forge
    options = df_unfiltered["forge"].unique().tolist()
    forges = st.multiselect("Filter by forge...", options, [])
    if forges:
        df = df[df["forge"].isin(forges)]

    # --- Owner
    options = df_unfiltered["owner"].unique().tolist()
    owners = st.multiselect("Filter by owner...", options, [])
    if owners:
        df = df[df["owner"].isin(owners)]


# Some cool metrics
st.header("Overview")
col1, col2, col3 = st.columns(3)
col1.metric("Compliant repos", len(df_unfiltered))
col2.metric("Cumulative stars", human_format(int(df_unfiltered["stars"].sum())))
col3.metric("Repos with 100+ stars", len(df_unfiltered.query("stars >= 100")))

col1, col2, col3 = st.columns(3)
col1.metric("Different forges", len(df_unfiltered["forge"].unique().tolist()))
col2.metric(
    "Total copyrighted files",
    human_format(int(df_unfiltered["copyrighted_files"].sum())),
)
col3.metric("Repos with 1000+ stars", len(df_unfiltered.query("stars >= 1000")))
# Repos by stars
st.header("Top 10 repos by stars")
st.markdown(
    """
You can use the sidebar on the left to filter this graph which always shows the
top 10 repos by stars depending on your chosen filter.
    """
)
df = df.sort_values(by="stars", ascending=False)
fig = px.bar(df[:10], x="url", y="stars", color="forge")
st.plotly_chart(fig, use_container_width=True)


# Used licenses
st.header("Distribution of licenses (top 10)")
df["used_licences"] = df["used_licences"].apply(lambda cell: transform_to_list(cell))
licenses_series = make_one_dimensional(df["used_licences"]).value_counts()
licenses_df = pd.DataFrame(licenses_series)
licenses_df = licenses_df.reset_index()
licenses_df.columns = ["license", "count"]
fig = px.pie(
    licenses_df[:10],
    values="count",
    names="license",
)
st.plotly_chart(fig, use_container_width=True)

# Repos by owner
st.header("Top 10 owners")
owners_series = df["owner"].value_counts()
owners_df = pd.DataFrame(owners_series)
owners_df = owners_df.reset_index()
owners_df.columns = ["owner", "number of repos"]
fig = px.pie(
    owners_df[:10],
    values="number of repos",
    names="owner",
)
st.plotly_chart(fig, use_container_width=True)

# Repos by forge
st.header("Top 10 forges")
forges_series = df["forge"].value_counts()
forges_df = pd.DataFrame(forges_series)
forges_df = forges_df.reset_index()
forges_df.columns = ["forge", "number of repos"]
fig = px.pie(
    forges_df[:10],
    values="number of repos",
    names="forge",
)
st.plotly_chart(fig, use_container_width=True)

# Raw data
st.header("Raw data")
st.dataframe(df)
